package com.sdasport.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "register")
public class Register {

	@Id
	private int registerId; 
	private String username; 
	private String password; 
	private String gender; 
	private String email;
	private String phone;
	
	
	public Register() {
		super();
	}

	public Register(int registerId, String username, String password, String gender, String email, String phone) {
		super();
		this.registerId = registerId;
		this.username = username;
		this.password = password;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
	}

	public int getRegisterId() {
		return registerId;
	}

	public void setRegisterId(int registerId) {
		this.registerId = registerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Register [registerId=" + registerId + ", username=" + username + ", password=" + password + ", gender="
				+ gender + ", email=" + email + ", phone=" + phone + "]";
	}
	
}
