package com.sdasport.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sport")
public class Sport {

	@Id
	private int sportId;
	private String sportName;
	private int maxPlayers;
	
	@OneToMany(mappedBy = "sport", fetch = FetchType.EAGER)
    private List<Location> location;
	
	@OneToMany(mappedBy = "sport")
    private List<Event> event;
	
	
	public Sport() {
		super();
	}

	public Sport(int sportId, String sportName, int maxPlayers) {
		super();
		this.sportId = sportId;
		this.sportName = sportName;
		this.maxPlayers = maxPlayers;
	}
	

	public int getSportId() {
		return sportId;
	}

	public void setSportId(int sportId) {
		this.sportId = sportId;
	}

	public String getSportName() {
		return sportName;
	}

	public void setSportName(String sportName) {
		this.sportName = sportName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	
	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}
	
	public List<Event> getEvent() {
		return event;
	}

	public void setEvent(List<Event> event) {
		this.event = event;
	}

	@Override
	public String toString() {
		return "Sport [sportId=" + sportId + ", sportName=" + sportName + ", maxPlayers=" + maxPlayers + "]";
	}
	
}
