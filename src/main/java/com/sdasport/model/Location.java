package com.sdasport.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "location")
public class Location {

	@Id
	private int locationId;
	private String address;
	private String locationName;
	private int tarif;
	
	@ManyToOne
    @JoinColumn(name = "facilitiesId")
    private Facilities facilities;
	
	@OneToMany(mappedBy = "location", fetch = FetchType.EAGER)
    private List<Scheduling> scheduling;

	@OneToMany(mappedBy = "location")
    private List<Event> event;
	
	@ManyToOne
    @JoinColumn(name = "sportId")
    private Sport sport;

	public Location() {
		super();
	}

	public Location(int locationId, String address, String locationName, int tarif) {
		super();
		this.locationId = locationId;
		this.address = address;
		this.locationName = locationName;
		this.tarif = tarif;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getTarif() {
		return tarif;
	}

	public void setTarif(int tarif) {
		this.tarif = tarif;
	}

	public Facilities getFacilities() {
		return facilities;
	}

	public void setFacilities(Facilities facilities) {
		this.facilities = facilities;
	}
	

	public List<Scheduling> getScheduling() {
		return scheduling;
	}

	public void setScheduling(List<Scheduling> scheduling) {
		this.scheduling = scheduling;
	}


	public List<Event> getEvent() {
		return event;
	}

	public void setEvent(List<Event> event) {
		this.event = event;
	}

	public Sport getSport() {
		return sport;
	}

	public void setSport(Sport sport) {
		this.sport = sport;
	}

	@Override
	public String toString() {
		return "Location [locationId=" + locationId + ", address=" + address + ", locationName=" + locationName
				 + ", tarif=" + tarif + "]";
	}

}
