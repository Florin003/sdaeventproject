package com.sdasport.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "equipment")
public class Equipment {
	
	@Id
	private int equipmentId;
	private String t_shirt; 
	private String vest; 
	private String pants;
	private String ball;
	
	@OneToMany(mappedBy = "equipment", fetch = FetchType.EAGER)
    private List<Facilities> facilities;
	
	public Equipment() {
		super();
	}

	public Equipment(int equipmentId, String t_shirt, String vest, String pants, String ball) {
		super();
		this.equipmentId = equipmentId;
		this.t_shirt = t_shirt;
		this.vest = vest;
		this.pants = pants;
		this.ball = ball;
	}

	public int getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}

	public String getT_shirt() {
		return t_shirt;
	}

	public void setT_shirt(String t_shirt) {
		this.t_shirt = t_shirt;
	}

	public String getVest() {
		return vest;
	}

	public void setVest(String vest) {
		this.vest = vest;
	}

	public String getPants() {
		return pants;
	}

	public void setPants(String pants) {
		this.pants = pants;
	}

	public String getBall() {
		return ball;
	}

	public void setBall(String ball) {
		this.ball = ball;
	}
	

	public List<Facilities> getFacilities() {
		return facilities;
	}

	public void setFacilities(List<Facilities> facilities) {
		this.facilities = facilities;
	}

	@Override
	public String toString() {
		return "Equipment [equipmentId=" + equipmentId + ", t_shirt=" + t_shirt + ", vest=" + vest + ", pants=" + pants
				+ ", ball=" + ball + "]";
	} 	
	
	
}
