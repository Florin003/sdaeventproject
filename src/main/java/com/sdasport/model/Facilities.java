package com.sdasport.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "facilities")
public class Facilities {

	@Id
	private int facilitiesId;
	private String parking;
	private String dressingRoom;
	private String showers;
	private String nocturne;
	private String wi_fi;
	
	@ManyToOne
    @JoinColumn(name = "equipmentId")
    private Equipment equipment;
	
	@OneToMany(mappedBy = "facilities", fetch = FetchType.EAGER)
    private List<Location> location;
	
	public Facilities() {
		super();
	}

	public Facilities(int facilitiesId, String parking, String dressingRoom, String showers, String nocturne,
			String wi_fi) {
		super();
		this.facilitiesId = facilitiesId;
		this.parking = parking;
		this.dressingRoom = dressingRoom;
		this.showers = showers;
		this.nocturne = nocturne;
		this.wi_fi = wi_fi;
	}

	public int getFacilitiesId() {
		return facilitiesId;
	}

	public void setFacilitiesId(int facilitiesId) {
		this.facilitiesId = facilitiesId;
	}

	public String getParking() {
		return parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getDressingRoom() {
		return dressingRoom;
	}

	public void setDressingRoom(String dressingRoom) {
		this.dressingRoom = dressingRoom;
	}

	public String getShowers() {
		return showers;
	}

	public void setShowers(String showers) {
		this.showers = showers;
	}

	public String getNocturne() {
		return nocturne;
	}

	public void setNocturne(String nocturne) {
		this.nocturne = nocturne;
	}

	public String getWi_fi() {
		return wi_fi;
	}

	public void setWi_fi(String wi_fi) {
		this.wi_fi = wi_fi;
	}
	

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Facilities [facilitiesId=" + facilitiesId + ", parking=" + parking + ", dressingRoom=" + dressingRoom
				+ ", showers=" + showers + ", nocturne=" + nocturne + ", wi_fi=" + wi_fi + "]";
	}
	



}
