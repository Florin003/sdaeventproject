package com.sdasport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_role")
public class UserRole {

	@Id
	private int userRoleId;

	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;

	@Column(name = "roleId")
	private int roleId;

	public UserRole() {
		super();
	}

	public UserRole(int userRoleId) {
		super();
		this.userRoleId = userRoleId;
	}

	public int getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}

	public User getLogin() {
		return user;
	}
	

	public void setLogin(User user) {
		this.user = user;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "UserRole [userRoleId=" + userRoleId + ", login=" + user + ", role=" + roleId + "]";
	}

}
