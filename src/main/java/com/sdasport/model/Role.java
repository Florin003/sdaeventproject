package com.sdasport.model;

public enum Role {
	ADMIN(1),
	CLIENT(2);
	
	private int id;

	Role(int id) {
		this.id = id;
	}
	
	public int getRoleId() {
		return id;
	}
}

