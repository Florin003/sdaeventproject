package com.sdasport.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "event")
public class Event {
	
	@Id
	private int eventId; 
	private String dateEventStart; 
	private String dateEventEnd;
	
	@ManyToOne
    @JoinColumn(name = "locationId")
    private Location location;
	
	@ManyToOne
    @JoinColumn(name = "sportId")
    private Sport sport;
	
	
	public Event() {
		super();
	}

	public Event(int eventId, String dateEventStart, String dateEventEnd) {
		super();
		this.eventId = eventId;
		this.dateEventStart = dateEventStart;
		this.dateEventEnd = dateEventEnd;
	}



	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getDateEventStart() {
		return dateEventStart;
	}

	public void setDateEventStart(String dateEventStart) {
		this.dateEventStart = dateEventStart;
	}

	public String getDateEventEnd() {
		return dateEventEnd;
	}

	public void setDateEventEnd(String dateEventEnd) {
		this.dateEventEnd = dateEventEnd;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Sport getSport() {
		return sport;
	}

	public void setSport(Sport sport) {
		this.sport = sport;
	}

	@Override
	public String toString() {
		return "Event [eventId=" + eventId + ", dateEventStart=" + dateEventStart + ", dateEventEnd=" + dateEventEnd
				+ "]";
	} 
	
	
}
