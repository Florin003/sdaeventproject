package com.sdasport.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;


@Entity
@Table(name = "scheduling")
public class Scheduling {

	@Id
	private int schedulingId;
	private String startTime;
	private String endTime;
	
	@ManyToOne
    @JoinColumn(name = "locationId")
    private Location location;
	
	public Scheduling() {
		super();
	}

	public Scheduling(int schedulingId, String startTime, String endTime) {
		super();
		this.schedulingId = schedulingId;
		this.startTime = startTime;
		this.endTime = endTime;

	}


	public int getSchedulingId() {
		return schedulingId;
	}

	public void setSchedulingId(int schedulingId) {
		this.schedulingId = schedulingId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Scheduling [schedulingId=" + schedulingId + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}

}
