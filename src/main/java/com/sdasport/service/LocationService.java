package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Location;

public interface LocationService {
	
	public List<Location> getAllLocation();
    
    public void saveLocation(int locationId, String address, String locationName, int tarif);

    public void deleteLocation(int locationId);

    public Location getLocationById(int locationId);

    public void updateLocation(int locationId, String address, String locationName, int tarif);
	 
}
