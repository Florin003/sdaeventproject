package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Register;
import com.sdasport.model.Role;

public interface RegisterService {

public List<Register> getAllRegister();
    
    public void saveRegister( String username, String password, String gender, String email, String phone);

    public void deleteRegister(int registerId);

    public Register getRegisterById(int registerId);

    public void updateRegister(int registerId, String username, String password, String gender, String email, String phone);
    
	
}
