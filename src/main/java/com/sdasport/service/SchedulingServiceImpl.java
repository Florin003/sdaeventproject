package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.SchedulingDao;
import com.sdasport.dao.SchedulingDaoImpl;
import com.sdasport.model.Scheduling;

@Service
@Transactional
public class SchedulingServiceImpl implements SchedulingService {

	@Autowired
	private SchedulingDao schedulingDao;
	
	@Override
	public List<Scheduling> getAllScheduling() {

		List<Scheduling> scheduling = schedulingDao.getAllScheduling();
		
		return scheduling;
	}

	@Override
	public void saveScheduling(int schedulingId, String startTime, String endTime) {

		Scheduling scheduling = new Scheduling();
		scheduling.setSchedulingId(schedulingId);
		scheduling.setStartTime(startTime);
		scheduling.setEndTime(endTime);
		
		schedulingDao.saveScheduling(scheduling);
	}

	@Override
	public void deleteScheduling(int schedulingId) {

		Scheduling scheduling = getSchedulingById(schedulingId);
		
			schedulingDao.deleteScheduling(scheduling);
	}

	@Override
	public Scheduling getSchedulingById(int schedulingId) {
		Scheduling scheduling = schedulingDao.getSchedulingById(schedulingId);
		
		return scheduling;
	}

	@Override
	public void updateScheduling(int schedulingId, String startTime, String endTime) {
		
		Scheduling scheduling = schedulingDao.getSchedulingById(schedulingId);
		
		scheduling.setSchedulingId(schedulingId);
		scheduling.setStartTime(startTime);
		scheduling.setEndTime(endTime);
		
	}
	
	public SchedulingDao getSchedulingDao() {
		return schedulingDao;
	}

	public void setSchedulingDao(SchedulingDaoImpl schedulingDao) {
		this.schedulingDao = schedulingDao;
	}

}
