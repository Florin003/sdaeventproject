package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.SportDao;
import com.sdasport.dao.SportDaoImpl;
import com.sdasport.model.Sport;

@Service
@Transactional
public class SportServiceImpl implements SportService {

	@Autowired
	private SportDao sportDao;
	
	@Override
	public List<Sport> getAllSport() {
		List<Sport> sport = sportDao.getAllSport();
		
		return sport;
	}

	@Override
	public void saveSport(int sportId, String sportName, int maxPlayers) {
		
		Sport sport = new Sport();
		sport.setSportId(sportId);
		sport.setSportName(sportName);
		sport.setMaxPlayers(maxPlayers);
		
		sportDao.saveSport(sport);
		
	}

	@Override
	public void deleteSport(int sportId) {
		Sport sport = getSportById(sportId);
		
		sportDao.deleteSport(sport);
		
	}

	@Override
	public Sport getSportById(int sportId) {
		Sport sport = sportDao.getSportById(sportId);
		
			return sport;
	}

	@Override
	public void updateSport(int sportId, String sportName, int maxPlayers) {
		Sport sport = sportDao.getSportById(sportId);
		
		sport.setSportId(sportId);
		sport.setSportName(sportName);
		sport.setMaxPlayers(maxPlayers);
	}
	
	public SportDao getSportDao() {
		return sportDao;
	}

	public void setSportDao(SportDaoImpl sportDao) {
		this.sportDao = sportDao;
	}

}
