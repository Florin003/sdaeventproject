package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.FacilitiesDao;
import com.sdasport.dao.FacilitiesDaoImpl;
import com.sdasport.dao.LocationDao;
import com.sdasport.dao.LocationDaoImpl;
import com.sdasport.dao.SportDao;
import com.sdasport.dao.SportDaoImpl;
import com.sdasport.model.Location;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationDao locationDao;
	
	@Autowired
	private FacilitiesDao facilitiesDao;
	
	@Autowired
	private SportDao sportDao;
	
	@Override
	public List<Location> getAllLocation() {
		List<Location> location = locationDao.getAllLocation();
		
		return location;
	}

	@Override
	public void saveLocation(int locationId, String address, String locationName, int tarif) {

		Location location = new Location();
		location.setLocationId(locationId);
		location.setAddress(address);
		location.setLocationName(locationName);
		location.setTarif(tarif);
	}

	@Override
	public void deleteLocation(int locationId) {
		
		Location location = getLocationById(locationId);
		
		locationDao.deleteLocation(location);
	}

	@Override
	public Location getLocationById(int locationId) {

		Location location = locationDao.getLocationById(locationId);
		
		return location;
	}

	@Override
	public void updateLocation(int locationId, String address, String locationName, int tarif) {
		
		Location location = locationDao.getLocationById(locationId);
		
		location.setLocationId(locationId);
		location.setAddress(address);
		location.setLocationName(locationName);
		location.setTarif(tarif);
	}
	public LocationDao getLocationDao() {
		return locationDao;
	}

	public void setLocationDao(LocationDaoImpl locationDao) {
		this.locationDao = locationDao;
	}
	public FacilitiesDao getFacilitiesDao() {
		return facilitiesDao;
	}

	public void setFacilitiesDao(FacilitiesDaoImpl facilitiesDao) {
		this.facilitiesDao = facilitiesDao;
	}
	
	public SportDao getSportDao() {
		return sportDao;
	}

	public void setSportDao(SportDaoImpl sportDao) {
		this.sportDao = sportDao;
	}
}
