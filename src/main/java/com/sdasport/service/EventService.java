package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Event;

public interface EventService {

public List<Event> getAllEvent();
    
    public void saveEvent(String sportName, String dateEventStart, String dateEventEnd, String nameLocation, String address, int maxPlayers);

    public void deleteEvent(int eventId);

    public Event getEventById(int eventId);

    public void updateEvent(int eventId, String dateEventStart, String dateEventEnd);
    
}

