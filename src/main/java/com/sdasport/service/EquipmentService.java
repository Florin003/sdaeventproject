package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Equipment;

public interface EquipmentService {

public List<Equipment> getAllEquipment();
    
    public void saveEquipment(int equipmentId, String t_shirt, String vest, String pants, String ball);

    public void deleteEquipment(int equipmentId);

    public Equipment getEquipmentById(int equipmentId);

    public void updateEquipment(int equipmentId, String t_shirt, String vest, String pants, String ball);
}
