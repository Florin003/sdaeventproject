package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Player;

public interface PlayerService {

public List<Player> getAllPlayer();
    
    public void savePlayer(int playerId, String firstName, String lastName, String passion, String phone_number);

    public void deletePlayer(int playerId);

    public Player getPlayerById(int playerId);

    public void updatePlayer(int playerId, String firstName, String lastName, String passion, String phone_number);
    
}
