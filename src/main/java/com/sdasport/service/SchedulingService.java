package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Scheduling;

public interface SchedulingService {

public List<Scheduling> getAllScheduling();
    
    public void saveScheduling(int schedulingId, String startTime, String endTime);

    public void deleteScheduling(int schedulingId);

    public Scheduling getSchedulingById(int schedulingId);

    public void updateScheduling(int schedulingId, String startTime, String endTime);
}
