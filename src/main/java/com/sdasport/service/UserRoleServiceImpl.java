package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.UserRoleDao;
import com.sdasport.model.UserRole;

@Service
@Transactional
public class UserRoleServiceImpl {

	@Autowired
	private UserRoleDao userRoleDao;

	public List<UserRole> getAllUserRole() {
		List<UserRole> userRole = userRoleDao.getAllUserRole();

		return userRole;
	}

	public void saveUserRole(UserRole userRole) {

		userRoleDao.saveUserRole(userRole);

	}
}
