package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.PlayerDao;
import com.sdasport.dao.PlayerDaoImpl;
import com.sdasport.model.Player;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

	@Autowired
	private PlayerDao playerDao;

	@Override
	public List<Player> getAllPlayer() {
		
		List <Player> player = playerDao.getAllPlayer();
		
		return player;
	}

	@Override
	public void savePlayer(int playerId, String firstName, String lastName, String passion, String phone_number) {
		
		Player player = new Player();
		player.setPlayerId(playerId);
		player.setFirstName(firstName);
		player.setLastName(lastName);
		player.setPassion(passion);
		player.setPhone_number(phone_number);
		
		playerDao.savePlayer(player);
	}

	@Override
	public void deletePlayer(int playerId) {
		
		Player player = getPlayerById(playerId);
		
		playerDao.deletePlayer(player);
	}

	@Override
	public Player getPlayerById(int playerId) {
		
		Player player = playerDao.getPlayerById(playerId);
		
		return player;
	}

	@Override
	public void updatePlayer(int playerId, String firstName, String lastName, String passion, String phone_number) {
		Player player = playerDao.getPlayerById(playerId);
		
		player.setPlayerId(playerId);
		player.setFirstName(firstName);
		player.setLastName(lastName);
		player.setPassion(passion);
		player.setPhone_number(phone_number);
		
		playerDao.updatePlayer(player);
		
	}
	public PlayerDao getPlayerDao() {
		return playerDao;
	}

	public void setPlayerDao(PlayerDaoImpl playerDao) {
		this.playerDao = playerDao;
	}
	
	
}
