package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Sport;

public interface SportService {
	
public List<Sport> getAllSport();
    
    public void saveSport(int sportId, String sportName, int maxPlayers);

    public void deleteSport(int sportId);

    public Sport getSportById(int sportId);

    public void updateSport(int sportId, String sportName, int maxPlayers);


}
