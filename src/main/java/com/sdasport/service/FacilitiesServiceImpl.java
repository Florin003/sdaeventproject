package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.FacilitiesDao;
import com.sdasport.dao.FacilitiesDaoImpl;
import com.sdasport.model.Facilities;

@Service
@Transactional
public class FacilitiesServiceImpl implements FacilitiesService {

	@Autowired
	private FacilitiesDao facilitiesDao;
	
	
	@Override
	public List<Facilities> getAllFacilities() {

		List<Facilities> facilities = facilitiesDao.getAllFacilities();
		
		return facilities;
	}

	@Override
	public void saveFacilities(int facilitiesId, String parking, String dressingRoom, String showers, String nocturne,
			String wi_fi) {
		
		Facilities facilities = new Facilities();
		facilities.setFacilitiesId(facilitiesId);
		facilities.setParking(parking);
		facilities.setDressingRoom(dressingRoom);
		facilities.setShowers(showers);
		facilities.setNocturne(nocturne);
		facilities.setWi_fi(wi_fi);
		
		facilitiesDao.saveFacilities(facilities);
	}

	@Override
	public void deleteFacilities(int facilitiesId) {

		Facilities facilities = getFacilitiesById(facilitiesId);
			
			facilitiesDao.deleteFacilities(facilities);
	}

	@Override
	public Facilities getFacilitiesById(int facilitiesId) {
		Facilities facilities = facilitiesDao.getFacilitiesById(facilitiesId);
		
		return facilities;
	}

	@Override
	public void updateFacilities(int facilitiesId, String parking, String dressingRoom, String showers, String nocturne,
			String wi_fi) {
		Facilities facilities = facilitiesDao.getFacilitiesById(facilitiesId);
		
		facilities.setParking(parking);
		facilities.setDressingRoom(dressingRoom);
		facilities.setShowers(showers);
		facilities.setNocturne(nocturne);
		facilities.setWi_fi(wi_fi);
	}

	public FacilitiesDao getFacilitiesDao() {
		return facilitiesDao;
	}

	public void setFacilitiesDao(FacilitiesDaoImpl facilitiesDao) {
		this.facilitiesDao = facilitiesDao;
	}
}
