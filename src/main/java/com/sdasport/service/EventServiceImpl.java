package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.EventDao;
import com.sdasport.dao.EventDaoImpl;
import com.sdasport.dao.LocationDao;
import com.sdasport.model.Event;
import com.sdasport.model.Location;
import com.sdasport.model.Sport;

@Service
@Transactional
public class EventServiceImpl implements EventService{

	@Autowired
	private EventDao eventDao;
	
	@Override
	public List<Event> getAllEvent() {
		
		List<Event> event = eventDao.getAllEvent();
		
		return event;
	}

	@Override
	public void saveEvent(String sportName, String dateEventStart, String dateEventEnd, String locationName, String address, int maxPlayers) {
		
		Sport sport = new Sport();
		sport.setSportName(sportName);
		sport.setMaxPlayers(maxPlayers);
		
		Location location = new Location();
		location.setLocationName(locationName);
		location.setAddress(address);
		
		Event event = new Event();
		event.setDateEventStart(dateEventStart);
		event.setDateEventEnd(dateEventEnd);
		
		eventDao.saveEvent(event);
	}

	@Override
	public void deleteEvent(int eventId) {
		
		Event event = eventDao.getEventById(eventId);
		
		eventDao.deleteEvent(event);
		
	}

	@Override
	public Event getEventById(int eventId) {
		
		Event event = eventDao.getEventById(eventId);
		
		return event;
		
	}

	@Override
	public void updateEvent(int eventId, String dateEventStart, String dateEventEnd) {
		
		Event event = eventDao.getEventById(eventId);
		
		event.setEventId(eventId);
		event.setDateEventStart(dateEventStart);
		event.setDateEventEnd(dateEventEnd);
		
		eventDao.updateEvent(event);
	}
	
	public EventDao getEventDao() {
		return eventDao;
	}

	public void setEventDao(EventDaoImpl eventDao) {
		this.eventDao = eventDao;
	}

}
