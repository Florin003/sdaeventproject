package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.LoginDao;
import com.sdasport.model.User;

@Service
@Transactional
public class LoginService {

	@Autowired
	private LoginDao loginDao;

	public List<User> getAllLogin() {
		List<User> user = loginDao.getAllLogin();

		return user;
	}

	public void saveLogin(int userId, String username, String password, String email) {

		User user = new User();
		user.setUserId(userId);
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);

		loginDao.saveLogin(user);

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int saveUser(User user) {

		return loginDao.saveLogin(user);
	}

	public User getLoginById(int loginId) {

		User login = loginDao.getLoginById(loginId);

		return login;
	}

}
