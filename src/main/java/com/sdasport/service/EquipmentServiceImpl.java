package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.EquipmentDao;
import com.sdasport.dao.EquipmentDaoImpl;
import com.sdasport.model.Equipment;

@Service
@Transactional
public class EquipmentServiceImpl implements EquipmentService {

	@Autowired
	private EquipmentDao equipmentDao;
	
	public List<Equipment> getAllEquipment() {
		
		List<Equipment> equipment = equipmentDao.getAllEquipment();
		
		return equipment;
	}

	public void saveEquipment(int equipmentId, String t_shirt, String vest, String pants, String ball) {
		
		Equipment equipment = new Equipment();
		equipment.setEquipmentId(equipmentId);
		equipment.setT_shirt(t_shirt);
		equipment.setVest(vest);
		equipment.setPants(pants);
		equipment.setBall(ball);
		
		equipmentDao.saveEquipment(equipment);
	}

	public void deleteEquipment(int equipmentId) {
		
		Equipment equipment = getEquipmentById(equipmentId);
		
		equipmentDao.deleteEquipment(equipment);
		
	}

	public Equipment getEquipmentById(int equipmentId) {
		Equipment equipment = equipmentDao.getEquipmentById(equipmentId);
		
				return equipment;
	}

	public void updateEquipment(int equipmentId, String t_shirt, String vest, String pants, String ball) {
		Equipment equipment = equipmentDao.getEquipmentById(equipmentId);
		
		equipment.setT_shirt(t_shirt);
		equipment.setVest(vest);
		equipment.setPants(pants);
		equipment.setBall(ball);
		
		equipmentDao.updateEquipment(equipment);
		
	}
	
	public EquipmentDao getEquipmentDao() {
		return equipmentDao;
	}

	public void setEquipmentDao(EquipmentDaoImpl equipmentDao) {
		this.equipmentDao = equipmentDao;
	}


}
