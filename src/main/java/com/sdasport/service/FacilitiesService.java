package com.sdasport.service;

import java.util.List;

import com.sdasport.model.Facilities;

public interface FacilitiesService {

public List<Facilities> getAllFacilities();
    
    public void saveFacilities(int facilitiesId, String parking, String dressingRoom, String showers, String nocturne, String wi_fi);

    public void deleteFacilities(int facilitiesId);

    public Facilities getFacilitiesById(int facilitiesId);

    public void updateFacilities(int facilitiesId, String parking, String dressingRoom, String showers, String nocturne, String wi_fi);
	
}
