package com.sdasport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdasport.dao.LoginDao;
import com.sdasport.dao.RegisterDao;
import com.sdasport.dao.RegisterDaoImpl;
import com.sdasport.dao.UserRoleDao;
import com.sdasport.model.User;
import com.sdasport.model.Register;
import com.sdasport.model.Role;
import com.sdasport.model.UserRole;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService{

	@Autowired
	private RegisterDao registerDao;
	
	@Autowired
	private LoginService loginServiceImpl;
	
	@Autowired
	private UserRoleServiceImpl userRoleService;
	
	@Override
	public List<Register> getAllRegister() {
		List<Register> register = registerDao.getAllRegister();
		
		return register;
	}

	@Override
	public void saveRegister(String username, String password, String gender, String email,
			String phone) {
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);
		
		
		Register register = new Register();
		register.setUsername(username);
		register.setPassword(password);
		register.setEmail(email);
		register.setPhone(phone);
		register.setGender(gender);
		
		registerDao.saveRegister(register);
		Integer userId = loginServiceImpl.saveUser(user);
		
		System.out.println(userId);
		
		UserRole userRole = new UserRole();
		userRole.setRoleId(Role.CLIENT.getRoleId());
		user = loginServiceImpl.getLoginById(userId);
		userRole.setUserRoleId(userId);		
		
		userRoleService.saveUserRole(userRole);

	}

	@Override
	public void deleteRegister(int registerId) {

	Register register = getRegisterById(registerId);
		
		registerDao.deleteRegister(register);
	}

	@Override
	public Register getRegisterById(int registerId) {
		Register register =registerDao.getRegisterById(registerId);
		
		return register;
	}

	@Override
	public void updateRegister(int registerId, String username, String password, String gender, String email,
			String phone) {
		
		Register register =registerDao.getRegisterById(registerId);
		register.setRegisterId(registerId);
		register.setUsername(username);
		register.setPassword(password);
		register.setGender(gender);
		register.setEmail(email);
		register.setPhone(phone);
		
		registerDao.updateRegister(register);
	}

	public RegisterDao getRegisterDao() {
		return registerDao;
	}

	public void setRegisterDao(RegisterDaoImpl registerDao) {
		this.registerDao = registerDao;
	}
}
