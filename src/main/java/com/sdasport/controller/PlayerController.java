package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdasport.model.Player;
import com.sdasport.service.PlayerService;


@Controller
@RequestMapping("/players")
public class PlayerController {
	
	@Autowired
	private PlayerService playerService;

	@RequestMapping("/list")
	public String playerList(ModelMap model) {
		List<Player> player = playerService.getAllPlayer();
		
		model.addAttribute("playerList", player);
		
		return "playerListView";
	}
	
}
