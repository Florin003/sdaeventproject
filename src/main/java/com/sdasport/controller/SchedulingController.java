package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdasport.model.Location;
import com.sdasport.model.Scheduling;
import com.sdasport.service.LocationService;
import com.sdasport.service.SchedulingService;

@Controller
@RequestMapping("/scheduling")
public class SchedulingController {

	@Autowired
	private SchedulingService schedulingService;
	
	@Autowired
	private LocationService locationService;
	
	@RequestMapping("/list")
	public String schedulingList(ModelMap model) {
		List<Scheduling> scheduling = schedulingService.getAllScheduling();
		List<Location> location = locationService.getAllLocation();
		
		model.addAttribute("schedulingList", scheduling);
		model.addAttribute("locationList", location);
		
		return "schedulingListView";
	}

}
