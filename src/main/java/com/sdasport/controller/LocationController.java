package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdasport.model.Facilities;
import com.sdasport.model.Location;
import com.sdasport.model.Sport;
import com.sdasport.service.FacilitiesService;
import com.sdasport.service.LocationService;
import com.sdasport.service.SportService;

@Controller
@RequestMapping("/location")
public class LocationController {

	@Autowired
	private LocationService locationService;
	
	@Autowired
	private FacilitiesService facilitiesService;
	
	@Autowired
	private SportService sportService;
	
	@RequestMapping("/list")
	public String locationList(ModelMap model) {
		List<Location> location = locationService.getAllLocation();
		List<Facilities> facilities = facilitiesService.getAllFacilities();
		List<Sport> sport = sportService.getAllSport();
		
		model.addAttribute("locationList", location);
		model.addAttribute("facilitiesList", facilities);
		model.addAttribute("sportList", sport);
		
		return "locationListView";
	}
}
