package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdasport.model.Register;
import com.sdasport.model.Role;
import com.sdasport.service.RegisterService;


@Controller
@RequestMapping("/register")
public class RegistrationController {

	@Autowired
    private RegisterService registerService;
	
	@RequestMapping("/list")
	public String registerList(ModelMap model) {
		List<Register> register = registerService.getAllRegister();
		
		model.addAttribute("registerList", register);
		
		return "Registration";
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createRegister(ModelMap model,
			@RequestParam("username") String username, @RequestParam("password") String password,
			@RequestParam("gender") String gender, @RequestParam("email") String email,
			@RequestParam("phone") String phone) {

		registerService.saveRegister(username, password, gender, email, phone);

		model.addAttribute("registerList", registerService.getAllRegister());

		return "loginView";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteRegister(ModelMap model, @RequestParam("registerId") int registerId) {

		try {

			registerService.deleteRegister(registerId);
		} catch (Exception e) {
			model.addAttribute("errMessage", "Can't delete register because it has person assigned to it");
		}

		model.addAttribute("registerList", registerService.getAllRegister());

		return "Registration";
	}

	
}
