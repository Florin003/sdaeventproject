package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdasport.model.Equipment;
import com.sdasport.service.EquipmentService;


@Controller
@RequestMapping("/equipment")
public class EquipmentController {

 	@Autowired
	private EquipmentService equipmentService;
	
 	@RequestMapping("/list")
 	public String listEquipment(ModelMap model) {
 		List<Equipment> equipment = equipmentService.getAllEquipment();
 		
 		model.addAttribute("equipmentList", equipment);
 		
		return "equipmentListView";
 	}
 	
 	
 	
 	
}
