package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdasport.model.Equipment;
import com.sdasport.model.Facilities;
import com.sdasport.service.EquipmentService;
import com.sdasport.service.FacilitiesService;


@Controller
@RequestMapping("/facilities")
public class FacilitiesController {

	@Autowired
	private FacilitiesService facilitiesService;
	
	@Autowired
	private EquipmentService equipmentService;
	
	@RequestMapping("/list") 
	public String listFacilities(ModelMap model) {
		List<Facilities> facilities = facilitiesService.getAllFacilities();
		List<Equipment> equipment = equipmentService.getAllEquipment();
		
		model.addAttribute("facilitiesList", facilities);
		model.addAttribute("equipmentList", equipment);
		
		return "facilitiesListView";
	}
	
	
}
