package com.sdasport.controller;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/")
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
		 
			return "loginView";
		 
	 }
	
	@RequestMapping(value = "/failedLogin", method = RequestMethod.GET)
    public String failedLogin(ModelMap model) {
		 model.addAttribute("msg", "Invalid credentials");
			return "loginView";
		 
	 }
	@RequestMapping(value = "/successfulLogin", method = RequestMethod.POST)
    public String successfulLogin(ModelMap model) {
		
		Authentication authentification = SecurityContextHolder.getContext().getAuthentication();
		
		for(GrantedAuthority authority : authentification.getAuthorities()) {
			if(authority.getAuthority().equals("ROLE_ADMIN"))
				return "redirect:/location/list";
		}
			return "redirect:/event/list";
		 
	 }
	
	@RequestMapping(value = "/accessDenied")
    public String accessDenied() {
			return "accessDenied";
		 
	 }
	
	 
}
