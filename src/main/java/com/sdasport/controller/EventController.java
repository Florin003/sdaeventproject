package com.sdasport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdasport.model.Event;
import com.sdasport.model.Location;
import com.sdasport.model.Sport;
import com.sdasport.service.EventService;
import com.sdasport.service.LocationService;
import com.sdasport.service.SportService;

@Controller
@RequestMapping("/event")
public class EventController {

	@Autowired
	private EventService eventService;
	
	@Autowired
	private LocationService locationService;

	@Autowired
	private SportService sportService;

	@RequestMapping("/list")
	public String listEvent(ModelMap model) {
		List<Event> event = eventService.getAllEvent();
		List<Location> location = locationService.getAllLocation();
		List<Sport> sport = sportService.getAllSport();

		model.addAttribute("eventList", event);
		model.addAttribute("locationList", location);
		model.addAttribute("sportList", sport);

		return "eventListView";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createEvent(ModelMap model,@RequestParam("sportName") String sportName, 
			@RequestParam("dateEventStart") String dateEventStart, @RequestParam("dateEventEnd") String dateEventEnd,
			@RequestParam("locationName") String locationName, @RequestParam("address") String address,
			@RequestParam("maxPlayers") int maxPlayers
			) {

		eventService.saveEvent(sportName, dateEventStart, dateEventEnd, locationName, address, maxPlayers);

		model.addAttribute("eventList", eventService.getAllEvent());

		return "eventListView";
	}
}
