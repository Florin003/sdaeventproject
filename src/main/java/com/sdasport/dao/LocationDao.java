package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Location;


public interface LocationDao {
	List<Location> getAllLocation();

	 void saveLocation(Location location);

	 void deleteLocation(Location location);

	 Location getLocationById(int locationId);

	 void updateLocation(Location location);
}
