package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Player;

public interface PlayerDao {
	List<Player> getAllPlayer();

	 void savePlayer(Player player);

	 void deletePlayer(Player player);

	 Player getPlayerById(int playerId);

	 void updatePlayer(Player player);
}
