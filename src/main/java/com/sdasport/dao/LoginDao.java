package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.User;


public interface LoginDao {

	 List<User> getAllLogin();

	 Integer saveLogin(User user);

	User getLoginById(int loginId);
}
