package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.UserRole;

public interface UserRoleDao {

	 List<UserRole> getAllUserRole();

	 void saveUserRole(UserRole userRole);

}
