package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.UserRole;

@Repository
public class UserRoleDaoImpl implements UserRoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<UserRole> getAllUserRole() {
		Query query = sessionFactory.getCurrentSession().createQuery("from UserRole");
		List<UserRole> userRole = query.getResultList();
		return userRole;
	}

	@Override
	public void saveUserRole(UserRole userRole) {
		sessionFactory.getCurrentSession().save(userRole);		
		
	}
	
	
}
