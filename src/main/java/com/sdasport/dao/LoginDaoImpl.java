package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Location;
import com.sdasport.model.User;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<User> getAllLogin() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Login");
		List<User> user = query.getResultList();
		
		return user;
	}

	@Override
	public Integer saveLogin(User user) {
		Integer id = (Integer) sessionFactory.getCurrentSession().save(user);
		System.out.println("id in dao " + id);
		return id;
	}
	
	@Override
	public User getLoginById(int loginId) {
		
		User user = sessionFactory.getCurrentSession().get(User.class, loginId);
		
		return user;
	}

}
