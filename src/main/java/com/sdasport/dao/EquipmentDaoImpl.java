package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.sdasport.model.Equipment;

@Repository
public class EquipmentDaoImpl implements EquipmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<Equipment> getAllEquipment() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Equipment");
		List<Equipment> equipment = query.getResultList();
		
		return equipment;
	}

	public void saveEquipment(Equipment equipment) {
		sessionFactory.getCurrentSession().save(equipment);		
	}

	public void deleteEquipment(Equipment equipment) {
		sessionFactory.getCurrentSession().delete(equipment);		
	}

	public Equipment getEquipmentById(int equipmentId) {

		Equipment equipment = sessionFactory.getCurrentSession().get(Equipment.class, equipmentId);

		return equipment;
	}

	public void updateEquipment(Equipment equipment) {
		sessionFactory.getCurrentSession().saveOrUpdate(equipment);
		
	}
	
	 
}
