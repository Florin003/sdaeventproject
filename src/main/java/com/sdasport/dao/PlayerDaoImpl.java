package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Player;

@Repository
public class PlayerDaoImpl implements PlayerDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Player> getAllPlayer() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Player");
		List<Player> player = query.getResultList();
		
		return player;
	}

	@Override
	public void savePlayer(Player player) {
		sessionFactory.getCurrentSession().save(player);			
		
	}

	@Override
	public void deletePlayer(Player player) {
		sessionFactory.getCurrentSession().delete(player);			
		
	}

	@Override
	public Player getPlayerById(int playerId) {
		
		Player player = sessionFactory.getCurrentSession().get(Player.class, playerId);
		
		return player;
		
	}

	@Override
	public void updatePlayer(Player player) {
		sessionFactory.getCurrentSession().saveOrUpdate(player);		
		
	}

}
