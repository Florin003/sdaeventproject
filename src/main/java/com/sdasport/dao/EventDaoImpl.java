package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Event;

@Repository
public class EventDaoImpl implements EventDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Event> getAllEvent() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Event");
		List<Event> event = query.getResultList();
		
		return event;
	}

	@Override
	public void saveEvent(Event event) {
		sessionFactory.getCurrentSession().save(event);				
		
	}

	@Override
	public void deleteEvent(Event event) {
		sessionFactory.getCurrentSession().delete(event);				
		
	}

	@Override
	public Event getEventById(int eventId) {
		
		Event event = sessionFactory.getCurrentSession().get(Event.class, eventId);
		
		return event;
	}

	@Override
	public void updateEvent(Event event) {
		sessionFactory.getCurrentSession().saveOrUpdate(event);		
		
	}

}
