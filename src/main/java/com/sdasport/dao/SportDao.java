package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Sport;

public interface SportDao {

	 List<Sport> getAllSport();

	 void saveSport(Sport sport);

	 void deleteSport(Sport sport);

	 Sport getSportById(int sportId);

	 void updateSport(Sport sport);
	
}
