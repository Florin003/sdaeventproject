package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Equipment;

public interface EquipmentDao {

	 List<Equipment> getAllEquipment();

	 void saveEquipment(Equipment equipment);

	 void deleteEquipment(Equipment equipment);

	 Equipment getEquipmentById(int equipmentId);

	 void updateEquipment(Equipment equipment);
}
