package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Facilities;


public interface FacilitiesDao {

	List<Facilities> getAllFacilities();

	 void saveFacilities(Facilities facilities);

	 void deleteFacilities(Facilities facilities);

	 Facilities getFacilitiesById(int facilitiesId);

	 void updateFacilities(Facilities facilities);

}
