package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Register;


public interface RegisterDao {
	
	List<Register> getAllRegister();

	 void saveRegister(Register register);

	 void deleteRegister(Register register);

	 Register getRegisterById(int registerId);

	 void updateRegister(Register register);

}
