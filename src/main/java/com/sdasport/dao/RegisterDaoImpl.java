package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Register;

@Repository
public class RegisterDaoImpl implements RegisterDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Register> getAllRegister() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Register");
		List<Register> register = query.getResultList();
		
		return register;
	}

	@Override
	public void saveRegister(Register register) {
		sessionFactory.getCurrentSession().save(register);			
		
	}

	@Override
	public void deleteRegister(Register register) {
		sessionFactory.getCurrentSession().delete(register);			
		
	}

	@Override
	public Register getRegisterById(int registerId) {
		
		Register register = sessionFactory.getCurrentSession().get(Register.class, registerId);
		
		return register;
	}

	@Override
	public void updateRegister(Register register) {
		sessionFactory.getCurrentSession().saveOrUpdate(register);		
		
	}

}
