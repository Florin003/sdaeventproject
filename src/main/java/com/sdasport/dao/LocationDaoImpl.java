package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Location;

@Repository
public class LocationDaoImpl implements LocationDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Location> getAllLocation() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Location");
		List<Location> location = query.getResultList();
		
		return location;
	}

	@Override
	public void saveLocation(Location location) {
		sessionFactory.getCurrentSession().save(location);			
	}

	@Override
	public void deleteLocation(Location location) {
		sessionFactory.getCurrentSession().delete(location);			
	}

	@Override
	public Location getLocationById(int locationId) {
		
		Location location = sessionFactory.getCurrentSession().get(Location.class, locationId);
		
		return location;
	}

	@Override
	public void updateLocation(Location location) {
		sessionFactory.getCurrentSession().saveOrUpdate(location);		
	}

}
