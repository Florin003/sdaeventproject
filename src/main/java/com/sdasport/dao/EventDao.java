package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Event;


public interface EventDao {

	List<Event> getAllEvent();

	 void saveEvent(Event event);

	 void deleteEvent(Event event);

	 Event getEventById(int eventId);

	 void updateEvent(Event event);
}
