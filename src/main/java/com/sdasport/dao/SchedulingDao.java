package com.sdasport.dao;

import java.util.List;

import com.sdasport.model.Scheduling;

public interface SchedulingDao {
	List<Scheduling> getAllScheduling();

	 void saveScheduling(Scheduling scheduling);

	 void deleteScheduling(Scheduling scheduling);

	 Scheduling getSchedulingById(int schedulingId);

	 void updateScheduling(Scheduling scheduling);
}
