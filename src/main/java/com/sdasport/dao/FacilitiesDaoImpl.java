package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Facilities;

@Repository
public class FacilitiesDaoImpl implements FacilitiesDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Facilities> getAllFacilities() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Facilities");
		List<Facilities> facilities = query.getResultList();
		
		return facilities;
	}

	@Override
	public void saveFacilities(Facilities facilities) {
		sessionFactory.getCurrentSession().save(facilities);				
	}

	@Override
	public void deleteFacilities(Facilities facilities) {
		sessionFactory.getCurrentSession().delete(facilities);				
	}

	@Override
	public Facilities getFacilitiesById(int facilitiesId) {
		Facilities facilities = sessionFactory.getCurrentSession().get(Facilities.class, facilitiesId);

		return facilities;
	}

	@Override
	public void updateFacilities(Facilities facilities) {
		sessionFactory.getCurrentSession().saveOrUpdate(facilities);		
	}

}
