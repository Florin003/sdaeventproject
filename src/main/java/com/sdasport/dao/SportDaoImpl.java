package com.sdasport.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sdasport.model.Sport;

@Repository
public class SportDaoImpl implements SportDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Sport> getAllSport() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Sport");
		List<Sport> sport = query.getResultList();
		
		return sport;
	}

	@Override
	public void saveSport(Sport sport) {
		sessionFactory.getCurrentSession().save(sport);			
		
	}

	@Override
	public void deleteSport(Sport sport) {
		sessionFactory.getCurrentSession().save(sport);			
		
	}

	@Override
	public Sport getSportById(int sportId) {

		Sport sport = sessionFactory.getCurrentSession().get(Sport.class, sportId);
		
		return sport;
	}

	@Override
	public void updateSport(Sport sport) {
		sessionFactory.getCurrentSession().saveOrUpdate(sport);		
		
	}
	
}
