<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<html>
<body style="background-image:url(http://www.pptbackgrounds.org/uploads/soccer-sports-backgrounds-wallpapers.jpg);">

<jsp:include page="index.jsp"></jsp:include>

<head>
<style>
table {
  border-collapse: collapse;
  width: 60%;
}

th, td {
  text-align: center;
  padding: 8px;
}

</style>
</head>

<center>

<h2 style="color:Green;">View all Event</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for sport..">
<table id="myTable" border="1">

		<tr>
			<td><h3 style="color:red;">Sport</h3></td>
			<td><h3 style="color:red;">Start Event</h3></td>
			<td><h3 style="color:red;">Stop Event</h3></td>
			<td><h3 style="color:red;">Location Name</h3></td>
			<td><h3 style="color:red;">Address</h3></td>
			<td><h3 style="color:red;">Max Players</h3></td>


		</tr>
		<c:forEach items="${eventList}" var="event">
			<tr>
				<td>${event.getSport().getSportName() }</td>
				<td>${event.getDateEventStart() }</td>
				<td>${event.getDateEventEnd() }</td>
				<td>${event.getLocation().getLocationName() }</td>
				<td>${event.getLocation().getAddress() }</td>
				<td>${event.getSport().getMaxPlayers() }</td>
				
				
</center>
				
			</tr>
		</c:forEach>
	</table>
	
	
	
<!--  	<select name="location">
    <c:forEach items="${locationList}" var="location">
        <option value="${location.locationId}">${location.locationName}</option>
    </c:forEach>
    </select>
	-->
	
					<h2 style="color:Green;">Add new Event</h2>
	
	<form method="post"
        action="${pageContext.request.contextPath}/event/create">
        Sport: <input type="text" name="sportName" > <br />  
        Start Event: <input type="datetime-local" name="dateEventStart" > <br /> 
        Stop Event: <input type="datetime-local" name="dateEventEnd" > <br /> 
        Location Name:<select name="location">
    <c:forEach items="${locationList}" var="location">
        <option value="${location.locationId}">${location.locationName}</option>
    </c:forEach>
    </select> <br />
        Address: <input type="text" name="address" > <br /> 
        Max Players: <select name="location">
    <c:forEach items="${sportList}" var="sport">
        <option value="${sport.sportId}">${sport.maxPlayers}</option>
    </c:forEach>
    </select> <br />
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
        <input type="submit" value="Save">
    </form>
</body>

</html>


<script>
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>


