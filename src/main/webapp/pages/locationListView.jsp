<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<html>
<body style="background-image:url(http://www.pptbackgrounds.org/uploads/soccer-sports-backgrounds-wallpapers.jpg);">

<jsp:include page="index.jsp"></jsp:include>

<head>
<style>
table {
  border-collapse: collapse;
  width: 60%;
}

th, td {
  text-align: center;
  padding: 8px;
}

</style>
</head>

<center>

<h2 style="color:Green;">View all Location</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for sport..">
<table id="myTable" border="1">

		<tr> 
	        <td><h3 style="color:red;">Sport</h3></td>
			<td><h3 style="color:red;">Address</h3></td>
			<td><h3 style="color:red;">Location Name</h3></td>
			<td><h3 style="color:red;">Tarif/Hour</h3></td>
			<td><h3 style="color:red;">Facilities</h3></td>
			
			
	
		</tr>
		<c:forEach items="${locationList}" var="location">
			<tr>

				<td>${location.getSport().getSportName() }</td>
				<td>${location.getAddress() }</td>
				<td>${location.getLocationName() }</td>
				<td>${location.getTarif() }</td>
				<td>${location.getFacilities().getFacilitiesId() }</td>
				

</center>
				
			</tr>
		</c:forEach>
	</table>
	
</body>

</html>

<script>
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>

