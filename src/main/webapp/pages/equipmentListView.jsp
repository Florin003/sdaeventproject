<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<html>
<body style="background-image:url(http://www.pptbackgrounds.org/uploads/soccer-sports-backgrounds-wallpapers.jpg);">

<jsp:include page="index.jsp"></jsp:include>

<head>
<style>
table {
  border-collapse: collapse;
  width: 60%;
}

th, td {
  text-align: center;
  padding: 8px;
}

</style>
</head>

<center>

<h2 style="color:Green;">View all Equipment</h2>

<table border="1">
		<tr>
			<td>EquipmentID</td>
			<td>T-shirt</td>
			<td>Vest</td>
			<td>Pants</td>
			<td>Ball</td>
			
	
		</tr>
		<c:forEach items="${equipmentList}" var="equipment">
			<tr>
				<td>${equipment.getEquipmentId() }</td>
				<td>${equipment.getT_shirt() }</td>
				<td>${equipment.getVest() }</td>
				<td>${equipment.getPants() }</td>
				<td>${equipment.getBall() }</td>

</center>
				
			</tr>
		</c:forEach>
	</table>
</body>

</html>
